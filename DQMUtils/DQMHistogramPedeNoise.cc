/*!
        \file                DQMHistogramPedeNoise.h
        \brief               base class to create and fill monitoring histograms
        \author              Fabio Ravera, Lorenzo Uplegger
        \version             1.0
        \date                6/5/19
        Support :            mail to : fabio.ravera@cern.ch
 */

#include "../DQMUtils/DQMHistogramPedeNoise.h"
#include "../HWDescription/ReadoutChip.h"
#include "../RootUtils/HistContainer.h"
#include "../RootUtils/RootContainerFactory.h"
#include "../Utils/ChannelContainerStream.h"
#include "../Utils/Container.h"
#include "../Utils/ContainerFactory.h"
#include "../Utils/ContainerStream.h"
#include "../Utils/EmptyContainer.h"
#include "../Utils/HybridContainerStream.h"
#include "../Utils/Occupancy.h"
#include "../Utils/ThresholdAndNoise.h"
#include "../Utils/Utilities.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"

//========================================================================================================================
DQMHistogramPedeNoise::DQMHistogramPedeNoise() {}

//========================================================================================================================
DQMHistogramPedeNoise::~DQMHistogramPedeNoise() {}

//========================================================================================================================
void DQMHistogramPedeNoise::book(TFile* theOutputFile, DetectorContainer& theDetectorStructure, const Ph2_System::SettingsMap& pSettingsMap)
{
    // find the maximum number of channels
    std::vector<size_t> cNChanls(0);
    for(auto cBrdIndx = 0; cBrdIndx < theDetectorStructure.size(); cBrdIndx++)
    {
        for(auto cOGIndx = 0; cOGIndx < theDetectorStructure.at(cBrdIndx)->size(); cOGIndx++)
        {
            for(auto cHybridIndx = 0; cHybridIndx < theDetectorStructure.at(cBrdIndx)->at(cOGIndx)->size(); cHybridIndx++)
            {
                for(auto cChipIndx = 0; cChipIndx < theDetectorStructure.at(cBrdIndx)->at(cOGIndx)->at(cHybridIndx)->size(); cChipIndx++)
                {
                    auto cNchnl = theDetectorStructure.at(cBrdIndx)->at(cOGIndx)->at(cHybridIndx)->at(cChipIndx)->size();
                    cNChanls.push_back(cNchnl);
                    // std::cout << __PRETTY_FUNCTION__ << "B" << +cBrdIndx << "OG" << +cOGIndx << "H" << +cHybridIndx << "C" << +cChipIndx << ":" << cNchnl << std::endl;
                }
            }
        }
    }
    // auto cMaxNChannels = std::max_element(std::begin(cNChanls), std::end(cNChanls));
    // auto cMinNChannels = std::min_element(std::begin(cNChanls), std::end(cNChanls));
    NCH = *std::max_element(std::begin(cNChanls), std::end(cNChanls)); // theDetectorStructure.at(0)->at(0)->at(0)->at(0)->size();
    // theDetectorStructure.at(0)->at(0)->at(0)->at(0)->size();
    // if
    // (static_cast<Ph2_HwDescription::ReadoutChip*>(theDetectorStructure.at(0)->at(0)->at(0)->at(0))->getFrontEndType()
    // == FrontEndType::SSA) NCH = NSSACHANNELS;

    auto cSetting = pSettingsMap.find("PlotSCurves");
    fPlotSCurves  = (cSetting != std::end(pSettingsMap)) ? boost::any_cast<double>(cSetting->second) : 0;
    cSetting      = pSettingsMap.find("FitSCurves");
    fFitSCurves   = (cSetting != std::end(pSettingsMap)) ? boost::any_cast<double>(cSetting->second) : 0;
    if(fFitSCurves) fPlotSCurves = true;

    ContainerFactory::copyStructure(theDetectorStructure, fDetectorData);

    // SCurve
    if(fPlotSCurves)
    {
        uint16_t nYbins = 1024;
        float    minY   = -0.5;
        float    maxY   = 1023.5;
        if(cWithSSA or cWithMPA)
        {
            nYbins = 255;
            minY   = -0.5;
            maxY   = 254.5;
        }

        HistContainer<TH2F> theTH2FSCurve("SCurve", "SCurve", NCH, -0.5, NCH - 0.5, nYbins, minY, maxY);
        RootContainerFactory::bookChipHistograms<HistContainer<TH2F>>(theOutputFile, theDetectorStructure, fDetectorSCurveHistograms, theTH2FSCurve);
        if(fFitSCurves)
        {
            HistContainer<TH1F> theTH1FSCurveContainer("SCurve", "SCurve", nYbins, minY, maxY);
            RootContainerFactory::bookChannelHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorChannelSCurveHistograms, theTH1FSCurveContainer);

            ContainerFactory::copyAndInitStructure<ThresholdAndNoise>(theDetectorStructure, fThresholdAndNoiseContainer);
        }
    }

    // Pedestal
    HistContainer<TH1F> theTH1FPedestalContainer("PedestalDistribution", "Pedestal Distribution", 2048, -0.5, 1023.5);
    RootContainerFactory::bookChipHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorPedestalHistograms, theTH1FPedestalContainer);

    // Noise
    HistContainer<TH1F> theTH1FNoiseContainer("NoiseDistribution", "Noise Distribution", 200, 0., 20.);
    RootContainerFactory::bookChipHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorNoiseHistograms, theTH1FNoiseContainer);

    // Strip Noise
    HistContainer<TH1F> theTH1FStripNoiseContainer("StripNoiseDistribution", "Strip Noise", NCH, -0.5, float(NCH) - 0.5);
    RootContainerFactory::bookChipHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorStripNoiseHistograms, theTH1FStripNoiseContainer);

    // Strip Pedestal
    HistContainer<TH1F> theTH1FStripPedestalContainer("StripPedestalDistribution", "Strip Pedestal", NCH, -0.5, float(NCH) - 0.5);
    RootContainerFactory::bookChipHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorStripPedestalHistograms, theTH1FStripPedestalContainer);

    // Strip Noise Even
    HistContainer<TH1F> theTH1FStripNoiseEvenContainer("StripNoiseEvenDistribution", "Strip Noise Even", NCH / 2, -0.5, float(NCH / 2) - 0.5);
    RootContainerFactory::bookChipHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorStripNoiseEvenHistograms, theTH1FStripNoiseEvenContainer);

    // Strip Noise Odd
    HistContainer<TH1F> theTH1FStripNoiseOddContainer("StripNoiseOddDistribution", "Strip Noise Odd", NCH / 2, -0.5, float(NCH / 2) - 0.5);
    RootContainerFactory::bookChipHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorStripNoiseOddHistograms, theTH1FStripNoiseOddContainer);

    // Pixel Noise Odd
    HistContainer<TH2F> theTH2F2DPixelNoiseContainer("2DPixelNoise", "Pixel Noise", 120, -0.5, float(120) - 0.5, NCH / 120, -0.5, float(NCH / 120) - 0.5);
    RootContainerFactory::bookChipHistograms<HistContainer<TH2F>>(theOutputFile, theDetectorStructure, fDetector2DPixelNoiseHistograms, theTH2F2DPixelNoiseContainer);

    // Hybrid Noise
    HistContainer<TH1F> theTH1FHybridNoiseContainer("HybridNoiseDistribution", "Hybrid Noise Distribution", 200, 0., 20.);
    RootContainerFactory::bookHybridHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorHybridNoiseHistograms, theTH1FHybridNoiseContainer);

    // Hybrid Strip Noise
    HistContainer<TH1F> theTH1FHybridStripNoiseContainer("HybridStripNoiseDistribution", "Hybrid Strip Noise", NCH * 8, -0.5, float(NCH) * 8 - 0.5);
    RootContainerFactory::bookHybridHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorHybridStripNoiseHistograms, theTH1FHybridStripNoiseContainer);

    // Hybrid Strip Even Noise
    HistContainer<TH1F> theTH1FHybridStripNoiseEvenContainer("HybridStripNoiseEvenDistribution", "Hybrid Strip Noise Even", NCH * 4, -0.5, float(NCH) * 4 - 0.5);
    RootContainerFactory::bookHybridHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorHybridStripNoiseEvenHistograms, theTH1FHybridStripNoiseEvenContainer);

    // Hybrid Strip Odd Noise
    HistContainer<TH1F> theTH1FHybridStripNoiseOddContainer("HybridStripNoiseOddDistribution", "Hybrid Strip Noise Odd", NCH * 4, -0.5, float(NCH) * 4 - 0.5);
    RootContainerFactory::bookHybridHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorHybridStripNoiseOddHistograms, theTH1FHybridStripNoiseOddContainer);

    // Validation
    HistContainer<TH1F> theTH1FValidationContainer("Occupancy", "Occupancy", NCH, -0.5, float(NCH) - 0.5);
    RootContainerFactory::bookChipHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorValidationHistograms, theTH1FValidationContainer);
}

//========================================================================================================================
bool DQMHistogramPedeNoise::fill(std::vector<char>& dataBuffer)
{
    HybridContainerStream<Occupancy, Occupancy, Occupancy> theOccupancy("PedeNoise");
    ChannelContainerStream<Occupancy, uint16_t>            theSCurve("PedeNoiseSCurve");
    ChannelContainerStream<ThresholdAndNoise>              theThresholdAndNoiseStream("PedeNoise");

    if(theOccupancy.attachBuffer(&dataBuffer))
    {
        std::cout << "Matched PedeNoise Occupancy!!!!!\n";
        theOccupancy.decodeData(fDetectorData);
        fillValidationPlots(fDetectorData);

        fDetectorData.cleanDataStored();
        return true;
    }
    else if(theSCurve.attachBuffer(&dataBuffer))
    {
        std::cout << "Matched PedeNoise SCurve!!!!!\n";
        theSCurve.decodeChipData(fDetectorData);
        fillSCurvePlots(theSCurve.getHeaderElement(), fDetectorData);

        fDetectorData.cleanDataStored();
        return true;
    }
    else if(theThresholdAndNoiseStream.attachBuffer(&dataBuffer))
    {
        std::cout << "Matched PedeNoise ThresholdAndNoise!!!!!\n";
        theThresholdAndNoiseStream.decodeChipData(fDetectorData);
        fillPedestalAndNoisePlots(fDetectorData);

        fDetectorData.cleanDataStored();
        return true;
    }

    return false;
}

//========================================================================================================================
void DQMHistogramPedeNoise::process()
{
    if(fFitSCurves) fitSCurves();

    for(auto board: fDetectorPedestalHistograms)
    {
        for(auto opticalGroup: *board)
        {
            for(auto hybrid: *opticalGroup)
            {
                TH1F* hybridStripNoiseEvenHistogram =
                    fDetectorHybridStripNoiseEvenHistograms.at(board->getIndex())->at(opticalGroup->getIndex())->at(hybrid->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
                TH1F* hybridStripNoiseOddHistogram =
                    fDetectorHybridStripNoiseOddHistograms.at(board->getIndex())->at(opticalGroup->getIndex())->at(hybrid->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
                hybridStripNoiseEvenHistogram->SetLineColor(kBlue);
                hybridStripNoiseEvenHistogram->SetMaximum(20);
                hybridStripNoiseEvenHistogram->SetMinimum(0);
                hybridStripNoiseOddHistogram->SetLineColor(kRed);
                hybridStripNoiseOddHistogram->SetMaximum(20);
                hybridStripNoiseOddHistogram->SetMinimum(0);
                hybridStripNoiseEvenHistogram->SetStats(false);
                hybridStripNoiseOddHistogram->SetStats(false);

                std::string validationCanvasName = "Validation_B_" + std::to_string(board->getId()) + "_O_" + std::to_string(opticalGroup->getId()) + "_H_" + std::to_string(hybrid->getId());
                std::string pedeNoiseCanvasName  = "PedeNoise_B_" + std::to_string(board->getId()) + "_O_" + std::to_string(opticalGroup->getId()) + "_H_" + std::to_string(hybrid->getId());

                TCanvas* cValidation = new TCanvas(validationCanvasName.data(), validationCanvasName.data(), 0, 0, 650, fPlotSCurves ? 900 : 650);
                TCanvas* cPedeNoise  = new TCanvas(pedeNoiseCanvasName.data(), pedeNoiseCanvasName.data(), 670, 0, 650, 650);

                cValidation->Divide(hybrid->size(), fPlotSCurves ? 3 : 2);
                cPedeNoise->Divide(hybrid->size(), 2);

                for(auto chip: *hybrid)
                {
                    cValidation->cd(chip->getIndex() + 1 + hybrid->size() * 0);
                    TH1F* validationHistogram = fDetectorValidationHistograms.at(board->getIndex())
                                                    ->at(opticalGroup->getIndex())
                                                    ->at(hybrid->getIndex())
                                                    ->at(chip->getIndex())
                                                    ->getSummary<HistContainer<TH1F>>()
                                                    .fTheHistogram;
                    validationHistogram->SetStats(false);
                    validationHistogram->DrawCopy();
                    gPad->SetLogy();

                    cValidation->cd(chip->getIndex() + 1 + hybrid->size() * 1);
                    TH1F* chipStripNoiseEvenHistogram = fDetectorStripNoiseEvenHistograms.at(board->getIndex())
                                                            ->at(opticalGroup->getIndex())
                                                            ->at(hybrid->getIndex())
                                                            ->at(chip->getIndex())
                                                            ->getSummary<HistContainer<TH1F>>()
                                                            .fTheHistogram;
                    TH1F* chipStripNoiseOddHistogram = fDetectorStripNoiseOddHistograms.at(board->getIndex())
                                                           ->at(opticalGroup->getIndex())
                                                           ->at(hybrid->getIndex())
                                                           ->at(chip->getIndex())
                                                           ->getSummary<HistContainer<TH1F>>()
                                                           .fTheHistogram;
                    chipStripNoiseEvenHistogram->SetLineColor(kBlue);
                    chipStripNoiseEvenHistogram->SetMaximum(20);
                    chipStripNoiseEvenHistogram->SetMinimum(0);
                    chipStripNoiseOddHistogram->SetLineColor(kRed);
                    chipStripNoiseOddHistogram->SetMaximum(20);
                    chipStripNoiseOddHistogram->SetMinimum(0);
                    chipStripNoiseEvenHistogram->SetStats(false);
                    chipStripNoiseOddHistogram->SetStats(false);
                    chipStripNoiseEvenHistogram->DrawCopy();
                    chipStripNoiseOddHistogram->DrawCopy("same");

                    cPedeNoise->cd(chip->getIndex() + 1 + hybrid->size() * 1);
                    fDetectorPedestalHistograms.at(board->getIndex())
                        ->at(opticalGroup->getIndex())
                        ->at(hybrid->getIndex())
                        ->at(chip->getIndex())
                        ->getSummary<HistContainer<TH1F>>()
                        .fTheHistogram->DrawCopy();

                    cPedeNoise->cd(chip->getIndex() + 1 + hybrid->size() * 0);
                    fDetectorNoiseHistograms.at(board->getIndex())
                        ->at(opticalGroup->getIndex())
                        ->at(hybrid->getIndex())
                        ->at(chip->getIndex())
                        ->getSummary<HistContainer<TH1F>>()
                        .fTheHistogram->DrawCopy();

                    if(fPlotSCurves)
                    {
                        TH2F* cSCurveHist = fDetectorSCurveHistograms.at(board->getIndex())
                                                ->at(opticalGroup->getIndex())
                                                ->at(hybrid->getIndex())
                                                ->at(chip->getIndex())
                                                ->getSummary<HistContainer<TH2F>>()
                                                .fTheHistogram;
                        TH1D* cTmp = cSCurveHist->ProjectionY();
                        cSCurveHist->GetYaxis()->SetRangeUser(cTmp->GetBinCenter(cTmp->FindFirstBinAbove(0)) - 10, cTmp->GetBinCenter(cTmp->FindLastBinAbove(0.99)) + 10);
                        // cSCurveHist->GetZaxis()->SetRangeUser(0,1.);
                        delete cTmp;
                        cValidation->cd(chip->getIndex() + 1 + hybrid->size() * 2);
                        cSCurveHist->SetStats(false);
                        cSCurveHist->DrawCopy("colz");
                    }

                    fDetectorStripNoiseHistograms.at(board->getIndex())
                        ->at(opticalGroup->getIndex())
                        ->at(hybrid->getIndex())
                        ->at(chip->getIndex())
                        ->getSummary<HistContainer<TH1F>>()
                        .fTheHistogram->GetYaxis()
                        ->SetRangeUser(0., 20.);
                    fDetectorStripNoiseEvenHistograms.at(board->getIndex())
                        ->at(opticalGroup->getIndex())
                        ->at(hybrid->getIndex())
                        ->at(chip->getIndex())
                        ->getSummary<HistContainer<TH1F>>()
                        .fTheHistogram->GetYaxis()
                        ->SetRangeUser(0., 20.);
                    fDetectorStripNoiseOddHistograms.at(board->getIndex())
                        ->at(opticalGroup->getIndex())
                        ->at(hybrid->getIndex())
                        ->at(chip->getIndex())
                        ->getSummary<HistContainer<TH1F>>()
                        .fTheHistogram->GetYaxis()
                        ->SetRangeUser(0., 20.);
                }

                fDetectorHybridStripNoiseHistograms.at(board->getIndex())
                    ->at(opticalGroup->getIndex())
                    ->at(hybrid->getIndex())
                    ->getSummary<HistContainer<TH1F>>()
                    .fTheHistogram->GetXaxis()
                    ->SetRangeUser(-0.5, NCH * hybrid->size() - 0.5);
                fDetectorHybridStripNoiseHistograms.at(board->getIndex())
                    ->at(opticalGroup->getIndex())
                    ->at(hybrid->getIndex())
                    ->getSummary<HistContainer<TH1F>>()
                    .fTheHistogram->GetYaxis()
                    ->SetRangeUser(0., 20.);

                fDetectorHybridStripNoiseEvenHistograms.at(board->getIndex())
                    ->at(opticalGroup->getIndex())
                    ->at(hybrid->getIndex())
                    ->getSummary<HistContainer<TH1F>>()
                    .fTheHistogram->GetXaxis()
                    ->SetRangeUser(-0.5, NCH * hybrid->size() - 0.5);
                fDetectorHybridStripNoiseEvenHistograms.at(board->getIndex())
                    ->at(opticalGroup->getIndex())
                    ->at(hybrid->getIndex())
                    ->getSummary<HistContainer<TH1F>>()
                    .fTheHistogram->GetYaxis()
                    ->SetRangeUser(0., 20.);

                fDetectorHybridStripNoiseOddHistograms.at(board->getIndex())
                    ->at(opticalGroup->getIndex())
                    ->at(hybrid->getIndex())
                    ->getSummary<HistContainer<TH1F>>()
                    .fTheHistogram->GetXaxis()
                    ->SetRangeUser(-0.5, NCH * hybrid->size() - 0.5);
                fDetectorHybridStripNoiseOddHistograms.at(board->getIndex())
                    ->at(opticalGroup->getIndex())
                    ->at(hybrid->getIndex())
                    ->getSummary<HistContainer<TH1F>>()
                    .fTheHistogram->GetYaxis()
                    ->SetRangeUser(0., 20.);
            }
        }
    }
}

//========================================================================================================================
void DQMHistogramPedeNoise::reset(void) {}

//========================================================================================================================
void DQMHistogramPedeNoise::fillValidationPlots(DetectorDataContainer& theOccupancy)
{
    for(auto board: theOccupancy)
    {
        for(auto opticalGroup: *board)
        {
            for(auto hybrid: *opticalGroup)
            {
                // std::cout << __PRETTY_FUNCTION__ << " The Hybrid Occupancy = " <<
                // hybrid->getSummary<Occupancy,Occupancy>().fOccupancy << std::endl;
                for(auto chip: *hybrid)
                {
                    TH1F* chipValidationHistogram = fDetectorValidationHistograms.at(board->getIndex())
                                                        ->at(opticalGroup->getIndex())
                                                        ->at(hybrid->getIndex())
                                                        ->at(chip->getIndex())
                                                        ->getSummary<HistContainer<TH1F>>()
                                                        .fTheHistogram;
                    uint channelBin = 1;

                    if(chip->getChannelContainer<Occupancy>() == nullptr) continue;
                    for(auto channel: *chip->getChannelContainer<Occupancy>())
                    {
                        chipValidationHistogram->SetBinContent(channelBin, channel.fOccupancy);
                        chipValidationHistogram->SetBinError(channelBin++, channel.fOccupancyError);
                    }
                }
            }
        }
    }
}

//========================================================================================================================
void DQMHistogramPedeNoise::fillPedestalAndNoisePlots(DetectorDataContainer& thePedestalAndNoise)
{
    for(auto board: thePedestalAndNoise)
    {
        for(auto opticalGroup: *board)
        {
            for(auto hybrid: *opticalGroup)
            {
                TH1F* hybridNoiseHistogram =
                    fDetectorHybridNoiseHistograms.at(board->getIndex())->at(opticalGroup->getIndex())->at(hybrid->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
                TH1F* hybridStripNoiseHistogram =
                    fDetectorHybridStripNoiseHistograms.at(board->getIndex())->at(opticalGroup->getIndex())->at(hybrid->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
                TH1F* hybridStripNoiseEvenHistogram =
                    fDetectorHybridStripNoiseEvenHistograms.at(board->getIndex())->at(opticalGroup->getIndex())->at(hybrid->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
                TH1F* hybridStripNoiseOddHistogram =
                    fDetectorHybridStripNoiseOddHistograms.at(board->getIndex())->at(opticalGroup->getIndex())->at(hybrid->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;

                for(auto chip: *hybrid)
                {
                    TH1F* chipPedestalHistogram =
                        fDetectorPedestalHistograms.at(board->getIndex())->at(opticalGroup->getIndex())->at(hybrid->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
                    TH1F* chipNoiseHistogram =
                        fDetectorNoiseHistograms.at(board->getIndex())->at(opticalGroup->getIndex())->at(hybrid->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
                    TH1F* chipStripNoiseHistogram = fDetectorStripNoiseHistograms.at(board->getIndex())
                                                        ->at(opticalGroup->getIndex())
                                                        ->at(hybrid->getIndex())
                                                        ->at(chip->getIndex())
                                                        ->getSummary<HistContainer<TH1F>>()
                                                        .fTheHistogram;
                    TH1F* chipStripPedestalHistogram = fDetectorStripPedestalHistograms.at(board->getIndex())
                                                           ->at(opticalGroup->getIndex())
                                                           ->at(hybrid->getIndex())
                                                           ->at(chip->getIndex())
                                                           ->getSummary<HistContainer<TH1F>>()
                                                           .fTheHistogram;
                    TH1F* chipStripNoiseEvenHistogram = fDetectorStripNoiseEvenHistograms.at(board->getIndex())
                                                            ->at(opticalGroup->getIndex())
                                                            ->at(hybrid->getIndex())
                                                            ->at(chip->getIndex())
                                                            ->getSummary<HistContainer<TH1F>>()
                                                            .fTheHistogram;
                    TH1F* chipStripNoiseOddHistogram = fDetectorStripNoiseOddHistograms.at(board->getIndex())
                                                           ->at(opticalGroup->getIndex())
                                                           ->at(hybrid->getIndex())
                                                           ->at(chip->getIndex())
                                                           ->getSummary<HistContainer<TH1F>>()
                                                           .fTheHistogram;

                    TH2F* chip2DPixelNoiseHistogram = fDetector2DPixelNoiseHistograms.at(board->getIndex())
                                                          ->at(opticalGroup->getIndex())
                                                          ->at(hybrid->getIndex())
                                                          ->at(chip->getIndex())
                                                          ->getSummary<HistContainer<TH2F>>()
                                                          .fTheHistogram;

                    if(chip->getChannelContainer<ThresholdAndNoise>() == nullptr) continue;
                    uint16_t channelNumber = 0;
                    for(auto channel: *chip->getChannelContainer<ThresholdAndNoise>())
                    {
                        float cNoise       = (std::isnan(channel.fNoise)) ? 666 : channel.fNoise;
                        float cNoiseErr    = (std::isnan(channel.fNoiseError)) ? 666 : channel.fNoiseError;
                        float cPedestal    = (std::isnan(channel.fThreshold)) ? 666 : channel.fThreshold;
                        float cPedestalErr = (std::isnan(channel.fThreshold)) ? 666 : channel.fThresholdError;
                        chipPedestalHistogram->Fill(cPedestal);
                        chipNoiseHistogram->Fill(cNoise);
                        hybridNoiseHistogram->Fill(cNoise);

                        chip2DPixelNoiseHistogram->SetBinContent(int(channelNumber % 120) + 1, int(channelNumber / 120) + 1, cNoise);
                        if((int(channelNumber) % 2) == 0)
                        {
                            chipStripNoiseEvenHistogram->SetBinContent(int(channelNumber / 2) + 1, cNoise);
                            chipStripNoiseEvenHistogram->SetBinError(int(channelNumber / 2) + 1, cNoiseErr);
                            hybridStripNoiseEvenHistogram->SetBinContent(NCHANNELS / 2 * chip->getId() + int(channelNumber / 2) + 1, cNoise);
                            hybridStripNoiseEvenHistogram->SetBinError(NCHANNELS / 2 * chip->getId() + int(channelNumber / 2) + 1, cNoiseErr);
                        }
                        else
                        {
                            chipStripNoiseOddHistogram->SetBinContent(int(channelNumber / 2) + 1, cNoise);
                            chipStripNoiseOddHistogram->SetBinError(int(channelNumber / 2) + 1, cNoiseErr);
                            hybridStripNoiseOddHistogram->SetBinContent(NCHANNELS / 2 * chip->getId() + int(channelNumber / 2) + 1, cNoise);
                            hybridStripNoiseOddHistogram->SetBinError(NCHANNELS / 2 * chip->getId() + int(channelNumber / 2) + 1, cNoiseErr);
                        }

                        chipStripNoiseHistogram->SetBinContent(channelNumber + 1, cNoise);
                        chipStripNoiseHistogram->SetBinError(channelNumber + 1, cNoiseErr);
                        chipStripPedestalHistogram->SetBinContent(channelNumber + 1, cPedestal);
                        chipStripPedestalHistogram->SetBinError(channelNumber + 1, cPedestalErr);
                        hybridStripNoiseHistogram->SetBinContent(NCHANNELS * chip->getId() + channelNumber + 1, cNoise);
                        hybridStripNoiseHistogram->SetBinError(NCHANNELS * chip->getId() + channelNumber + 1, cNoiseErr);

                        if((int(channelNumber) % 2) == 0)
                        {
                            chipStripNoiseEvenHistogram->SetBinContent(int(channelNumber / 2) + 1, cNoise);
                            chipStripNoiseEvenHistogram->SetBinError(int(channelNumber / 2) + 1, cNoiseErr);
                            hybridStripNoiseEvenHistogram->SetBinContent(NCH / 2 * chip->getIndex() + int(channelNumber / 2) + 1, cNoise);
                            hybridStripNoiseEvenHistogram->SetBinError(NCH / 2 * chip->getIndex() + int(channelNumber / 2) + 1, cNoiseErr);
                        }
                        else
                        {
                            chipStripNoiseOddHistogram->SetBinContent(int(channelNumber / 2) + 1, cNoise);
                            chipStripNoiseOddHistogram->SetBinError(int(channelNumber / 2) + 1, cNoiseErr);
                            hybridStripNoiseOddHistogram->SetBinContent(NCH / 2 * chip->getIndex() + int(channelNumber / 2) + 1, cNoise);
                            hybridStripNoiseOddHistogram->SetBinError(NCH / 2 * chip->getIndex() + int(channelNumber / 2) + 1, cNoiseErr);
                        }

                        chipStripNoiseHistogram->SetBinContent(channelNumber + 1, cNoise);
                        chipStripNoiseHistogram->SetBinError(channelNumber + 1, cNoiseErr);
                        chipStripPedestalHistogram->SetBinContent(channelNumber + 1, channel.fThreshold);
                        chipStripPedestalHistogram->SetBinError(channelNumber + 1, channel.fThresholdError);
                        hybridStripNoiseHistogram->SetBinContent(NCH * chip->getIndex() + channelNumber + 1, cNoise);
                        hybridStripNoiseHistogram->SetBinError(NCH * chip->getIndex() + channelNumber + 1, cNoiseErr);
                        ++channelNumber;
                    }
                }
            }
        }
    }
}

//========================================================================================================================
void DQMHistogramPedeNoise::fillSCurvePlots(uint16_t vcthr, DetectorDataContainer& fSCurveOccupancy)
{
    for(auto board: fSCurveOccupancy)
    {
        for(auto opticalGroup: *board)
        {
            for(auto hybrid: *opticalGroup)
            {
                for(auto chip: *hybrid)
                {
                    TH2F* chipSCurve =
                        fDetectorSCurveHistograms.at(board->getIndex())->at(opticalGroup->getIndex())->at(hybrid->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH2F>>().fTheHistogram;

                    if(chip->getChannelContainer<Occupancy>() == nullptr) continue;
                    uint16_t channelNumber = 0;
                    for(auto channel: *chip->getChannelContainer<Occupancy>())
                    {
                        float tmpOccupancy      = channel.fOccupancy;
                        float tmpOccupancyError = channel.fOccupancyError;
                        chipSCurve->SetBinContent(channelNumber + 1, vcthr + 1, tmpOccupancy);
                        chipSCurve->SetBinError(channelNumber + 1, vcthr + 1, tmpOccupancyError);

                        if(fFitSCurves)
                        {
                            TH1F* channelSCurve = fDetectorChannelSCurveHistograms.at(board->getIndex())
                                                      ->at(opticalGroup->getIndex())
                                                      ->at(hybrid->getIndex())
                                                      ->at(chip->getIndex())
                                                      ->getChannel<HistContainer<TH1F>>(channelNumber)
                                                      .fTheHistogram;
                            channelSCurve->SetBinContent(vcthr + 1, tmpOccupancy);
                            channelSCurve->SetBinError(vcthr + 1, tmpOccupancyError);
                        }
                        ++channelNumber;
                    }
                }
            }
        }
    }
}

void DQMHistogramPedeNoise::fillSCurvePlots(DetectorDataContainer& fThresholds, DetectorDataContainer& fSCurveOccupancy)
{
    for(auto board: fSCurveOccupancy)
    {
        auto& cThThisBrd = fThresholds.at(board->getIndex());
        for(auto opticalGroup: *board)
        {
            auto& cThThisGrp = cThThisBrd->at(opticalGroup->getIndex());
            for(auto hybrid: *opticalGroup)
            {
                auto& cThThisHybrd = cThThisGrp->at(hybrid->getIndex());
                for(auto chip: *hybrid)
                {
                    auto& cThThisChip = cThThisHybrd->at(chip->getIndex());
                    TH2F* chipSCurve =
                        fDetectorSCurveHistograms.at(board->getIndex())->at(opticalGroup->getIndex())->at(hybrid->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH2F>>().fTheHistogram;

                    if(chip->getChannelContainer<Occupancy>() == nullptr) continue;
                    uint16_t channelNumber = 0;
                    for(auto channel: *chip->getChannelContainer<Occupancy>())
                    {
                        float tmpOccupancy      = channel.fOccupancy;
                        float tmpOccupancyError = channel.fOccupancyError;
                        chipSCurve->SetBinContent(channelNumber + 1, cThThisChip->getSummary<uint16_t>() + 1, tmpOccupancy);
                        chipSCurve->SetBinError(channelNumber + 1, cThThisChip->getSummary<uint16_t>() + 1, tmpOccupancyError);

                        if(fFitSCurves)
                        {
                            TH1F* channelSCurve = fDetectorChannelSCurveHistograms.at(board->getIndex())
                                                      ->at(opticalGroup->getIndex())
                                                      ->at(hybrid->getIndex())
                                                      ->at(chip->getIndex())
                                                      ->getChannel<HistContainer<TH1F>>(channelNumber)
                                                      .fTheHistogram;
                            channelSCurve->SetBinContent(cThThisChip->getSummary<uint16_t>() + 1, tmpOccupancy);
                            channelSCurve->SetBinError(cThThisChip->getSummary<uint16_t>() + 1, tmpOccupancyError);
                        }
                        ++channelNumber;
                    }
                }
            }
        }
    }
}

//========================================================================================================================
void DQMHistogramPedeNoise::fitSCurves()
{
    for(auto board: fDetectorChannelSCurveHistograms)
    {
        for(auto opticalGroup: *board)
        {
            for(auto hybrid: *opticalGroup)
            {
                for(auto chip: *hybrid)
                {
                    ChipDataContainer* theChipThresholdAndNoise = fThresholdAndNoiseContainer.at(board->getIndex())->at(opticalGroup->getIndex())->at(hybrid->getIndex())->at(chip->getIndex());

                    for(uint32_t cChannel = 0; cChannel < NCH; cChannel++)
                    {
                        TH1F* channelSCurve = chip->getChannel<HistContainer<TH1F>>(cChannel).fTheHistogram;

                        float channelNoise = fDetectorStripNoiseHistograms.at(board->getIndex())
                                                 ->at(opticalGroup->getIndex())
                                                 ->at(hybrid->getIndex())
                                                 ->at(chip->getIndex())
                                                 ->getSummary<HistContainer<TH1F>>()
                                                 .fTheHistogram->GetBinContent(cChannel + 1);

                        float channelPedestal = fDetectorStripPedestalHistograms.at(board->getIndex())
                                                    ->at(opticalGroup->getIndex())
                                                    ->at(hybrid->getIndex())
                                                    ->at(chip->getIndex())
                                                    ->getSummary<HistContainer<TH1F>>()
                                                    .fTheHistogram->GetBinContent(cChannel + 1);

                        TF1* cFit = new TF1("SCurveFit", MyErf, channelPedestal - (channelNoise * 5), channelPedestal + (channelNoise * 5), 2);

                        cFit->SetParameter(0, channelPedestal);
                        cFit->SetParameter(1, channelNoise);

                        // Fit
                        channelSCurve->Fit(cFit, "RQ+0");

                        theChipThresholdAndNoise->getChannel<ThresholdAndNoise>(cChannel).fThreshold      = cFit->GetParameter(0);
                        theChipThresholdAndNoise->getChannel<ThresholdAndNoise>(cChannel).fNoise          = cFit->GetParameter(1);
                        theChipThresholdAndNoise->getChannel<ThresholdAndNoise>(cChannel).fThresholdError = cFit->GetParError(0);
                        theChipThresholdAndNoise->getChannel<ThresholdAndNoise>(cChannel).fNoiseError     = cFit->GetParError(1);
                    }
                }
            }
        }
    }

    fillPedestalAndNoisePlots(fThresholdAndNoiseContainer);
}
